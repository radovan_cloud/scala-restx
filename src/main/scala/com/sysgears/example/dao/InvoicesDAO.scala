package com.sysgears.example.dao

import java.sql._

import com.sysgears.example.config.Configuration
import com.sysgears.example.domain._

import scala.slick.driver.MySQLDriver.simple.Database.threadLocalSession
import scala.slick.driver.MySQLDriver.simple._
import scala.slick.jdbc.meta.MTable

/**
 * Provides DAL for Customer entities for MySQL database.
 */
class InvoicesDAO extends Configuration {

  // init Database instance
  private val db = Database.forURL(url = "jdbc:mysql://%s:%d/%s".format(dbHost, dbPort, dbName),
    user = dbUser, password = dbPassword, driver = "com.mysql.jdbc.Driver")

  // create tables if not exist
  db.withSession {
    if (MTable.getTables("invoices").list().isEmpty) {
      Invoices.ddl.create
    }
  }

  /**
   * Saves customer entity into database.
   *
   * @param invoice customer entity to
   * @return saved customer entity
   */
  def create(invoice: Invoice): Either[Failure, Invoice] = {
    try {
      val id = db.withSession {
        Invoices returning Invoices.id insert invoice
      }
      Right(invoice.copy(id = Some(id)))
    } catch {
      case e: SQLException =>
        Left(databaseError(e))
    }
  }

  /**
   * Updates customer entity with specified one.
   *
   * @param id       id of the customer to update.
   * @param invoice updated customer entity
   * @return updated customer entity
   */
  def update(id: Long, invoice: Invoice): Either[Failure, Invoice] = {
    try
      db.withSession {
        Invoices.where(_.id === id) update invoice.copy(id = Some(id)) match {
          case 0 => Left(notFoundError(id))
          case _ => Right(invoice.copy(id = Some(id)))
        }
      }
    catch {
      case e: SQLException =>
        Left(databaseError(e))
    }
  }

  /**
   * Deletes customer from database.
   *
   * @param id id of the customer to delete
   * @return deleted customer entity
   */
  def delete(id: Long): Either[Failure, Invoice] = {
    try {
      db.withTransaction {
        val query = Invoices.where(_.id === id)
        val invoices = query.run.asInstanceOf[List[Invoice]]
        invoices.size match {
          case 0 =>
            Left(notFoundError(id))
          case _ => {
            query.delete
            Right(invoices.head)
          }
        }
      }
    } catch {
      case e: SQLException =>
        Left(databaseError(e))
    }
  }

  /**
   * Retrieves specific customer from database.
   *
   * @param id id of the customer to retrieve
   * @return customer entity with specified id
   */
  def get(id: Long): Either[Failure, Invoice] = {
    try {
      db.withSession {
        Invoices.findById(id).firstOption match {
          case Some(invoice: Invoice) =>
            Right(invoice)
          case _ =>
            Left(notFoundError(id))
        }
      }
    } catch {
      case e: SQLException =>
        Left(databaseError(e))
    }
  }

  /**
   * Retrieves list of customers with specified parameters from database.
   *
   * @param params search parameters
   * @return list of customers that match given parameters
   */
  def search(params: InvoiceSearchParameters): Either[Failure, List[Invoice]] = {
    implicit val typeMapper = Invoices.dateTypeMapper

    try {
      db.withSession {
        val query = for {
          invoice <- Invoices if {
          Seq(
            params.invoiceName.map(invoice.invoiceName is _),
            params.invoiceCity.map(invoice.invoiceCity is _),
            params.invoiceDate.map(invoice.invoiceDate is _)
          ).flatten match {
            case Nil => ConstColumn.TRUE
            case seq => seq.reduce(_ && _)
          }
        }
        } yield invoice

        Right(query.run.toList)
      }
    } catch {
      case e: SQLException =>
        Left(databaseError(e))
    }
  }

  /**
   * Produce database error description.
   *
   * @param e SQL Exception
   * @return database error description
   */
  protected def databaseError(e: SQLException) =
    Failure("%d: %s".format(e.getErrorCode, e.getMessage), FailureType.DatabaseFailure)

  /**
   * Produce customer not found error description.
   *
   * @param id id of the customer
   * @return not found error description
   */
  protected def notFoundError(invoiceId: Long) =
    Failure("Invoice with id=%d does not exist".format(invoiceId), FailureType.NotFound)
}