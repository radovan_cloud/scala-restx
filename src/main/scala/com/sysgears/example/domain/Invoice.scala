package com.sysgears.example.domain

import scala.slick.driver.MySQLDriver.simple._

/**
 * Customer entity.
 *
 * @param id        unique id
 * @param invoiceName first name
 * @param invoiceCity  last name
 * @param invoiceDate  date of birth
 */
case class Invoice(id: Option[Long], invoiceName: String, invoiceCity: String, invoiceDate: Option[java.util.Date])

/**
 * Mapped customers table object.
 */
object Invoices extends Table[Invoice]("invoices") {

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

  def invoiceName = column[String]("name")

  def invoiceCity = column[String]("city")

  def invoiceDate = column[java.util.Date]("dateinv", O.Nullable)



  def * = id.? ~ invoiceName ~ invoiceCity ~ invoiceDate.? <>(Invoice, Invoice.unapply _)

  implicit val dateTypeMapper = MappedTypeMapper.base[java.util.Date, java.sql.Date](
  {
    ud => new java.sql.Date(ud.getTime)
  }, {
    sd => new java.util.Date(sd.getTime)
  })

  val findById = for {
    id <- Parameters[Long]
    c <- this if c.id is id
  } yield c
}