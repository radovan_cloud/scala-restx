package com.sysgears.example.domain

import java.util.Date

/**
 * Customers search parameters.
 *
 * @param invoiceName first name
 * @param invoiceCity  last name
 * @param invoiceDate  date of birth
 */
case class InvoiceSearchParameters(invoiceName: Option[String] = None,
                                   invoiceCity: Option[String] = None,
                                   invoiceDate: Option[Date] = None)